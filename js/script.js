var firstNumb = $('.firstNumb'),
 secondNumb = $('.secondNumb'),
 resultField = $('.inputField'),
 hiddenField = $('.hiddenField'),
 secondNumberField = $('.secondNumberField'),
 fieldsCalc = $('.fieldsCalc'),
 showInputns = $('.showInputns'),
 showInputsSqrt = $('.showInputsSqrt'),
 sqrtFields = $('.sqrtFields');

showInputns.on('click',function(){
    fieldsCalc.slideToggle();
});
showInputsSqrt.on('click',function(){
   fieldsCalc.slideToggle('medium', function() {
        secondNumberField.toggleClass('hideClass');
        sqrtFields.css("justify-content", "center");
    
    });
});
 
function addNumbers () {
    var sum = Number(firstNumb.val()) + Number(secondNumb.val());
    resultField.val(sum);
 
}
function subtractNumbers() {
    var sum = Number(firstNumb.val()) - Number(secondNumb.val());
    resultField.val(sum);
}
function multiplication() {
    var sum = Number(firstNumb.val()) * Number(secondNumb.val());
    resultField.val(sum);
}
function sqrt() {
    var sum = Math.sqrt(Number(firstNumb.val()));
    resultField.val(sum);
}
function percentage() {
    var sum = ( Number(firstNumb.val()) * Number(secondNumb.val()) ) / 100;
    resultField.val(sum);
}
function potega() {
    var sum = Math.pow( Number(firstNumb.val()) ,  Number(secondNumb.val()) );
    resultField.val(sum);
}

function clearNumbers () {
    firstNumb = $('.firstNumb').val("");
    secondNumb = $('.secondNumb').val("");
    resultField = $('.inputField').val("");
}


